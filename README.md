# Glowin Data

This repository contains scatter-diagram of significant wave height ($H_s$) and wave period ($T0m1$) for each zone of the globe. 


<figure>
  <img src="pictures/density_with_zones.png" alt="alt text" width="500"/>
  <figcaption> GWS zone </figcaption>
</figure>



Example for zone 9 :

<figure>
  <img src="pictures/9_Routed.png" alt="alt text" width="500"/>
  <figcaption>Scatter-diagram for zone 9, including effect of ship routing.</figcaption>
</figure>


The detailed technical background is described [here](https://www.sciencedirect.com/science/article/pii/S0141118722003674).




